var express = require('express');
var app = express();

app.use(express.static('src'));
app.use(express.static('graph'));

app.get('*', function(req, res) {
  res.status(404).end('Error, cannot find a file in src');
})

app.listen('3000', function() {
  console.log('listening on 3000');
})
