

/* Inspired by Lee Byron's test data generator. */
function stream_layers(n, m, o) {
  if (arguments.length < 3) o = 0;
  function bump(a) {
    var x = 1 / (.1 + Math.random()),
        y = 2 * Math.random() - .5,
        z = 10 / (.1 + Math.random());
    for (var i = 0; i < m; i++) {
      var w = (i / m - y) * z;
      a[i] += x * Math.exp(-w * w);
    }
  }
  return d3.range(n).map(function() {
      var a = [], i;
      for (i = 0; i < m; i++) a[i] = o + o * Math.random();
      for (i = 0; i < 5; i++) bump(a);
      return a.map(stream_index);
    });
}

/* Another layer generator using gamma distributions. */
function stream_waves(n, m) {
  return d3.range(n).map(function(i) {
    return d3.range(m).map(function(j) {
        var x = 20 * j / m - i / 3;
        return 2 * x * Math.exp(-.5 * x);
      }).map(stream_index);
    });
}

function stream_index(d, i) {
  return {x: i, y: Math.max(0, d)};
}


nv.addGraph(function() {
  var chart = nv.models.lineWithFocusChart();
  /* WILO: how do I change the values on a "focus" chart? */

  chart.xAxis
    .tickFormat((d) => {
      return d3.time.format('%b %Y')(new Date(d))
    } );
      // .tickFormat(d3.format(',f'));

  chart.yAxis
      .tickFormat(d3.format(',.2f'));

  chart.color(['#F3302A']);

  d3.select('#chart svg')
      .datum(testData())
      .transition().duration(500)
      .call(chart);

  nv.utils.windowResize(chart.update);

  return chart;
});
/**************************************
 * Simple test data generator
 */

var p = d3.selectAll('p')
  .data([{d: 4},{d: 4},{d: 4},{d: 4},{d: 4}])
  .text(function(d) { return "I’m number " + d.d + "!"; });

p.enter().append("p")
  .text(function(d) { return "Extra number: " + d.d + "!"; });

function testData() {
  console.log(data[0])
  var values = data.map(function(data, i) {
    return {
      x: new Date(data.Date).getTime(), y: data.Actual
    }
  });

  return [{
    key: 'Платёжный баланс',
    values: values
  }];

  /*return stream_layers(1,128,.1).map(function(data, i) {
    return {
      key: 'Stream' + i,
      values: data
    };
  });*/
}

/*var svg = d3.select("svg"),
    margin = {top: 20, right: 20, bottom: 110, left: 40},
    margin2 = {top: 430, right: 20, bottom: 30, left: 40},
    width = +svg.attr("width") - margin.left - margin.right,
    height = +svg.attr("height") - margin.top - margin.bottom,
    height2 = +svg.attr("height") - margin2.top - margin2.bottom;

var parseDate = d3.timeParse("%b %Y");

var x = d3.scaleTime().range([0, width]),
    x2 = d3.scaleTime().range([0, width]),
    y = d3.scaleLinear().range([height, 0]),
    y2 = d3.scaleLinear().range([height2, 0]);

var xAxis = d3.axisBottom(x),
    xAxis2 = d3.axisBottom(x2),
    yAxis = d3.axisLeft(y);

var brush = d3.brushX()
    .extent([[0, 0], [width, height2]])
    .on("brush end", brushed);

var zoom = d3.zoom()
    .scaleExtent([1, Infinity])
    .translateExtent([[0, 0], [width, height]])
    .extent([[0, 0], [width, height]])
    .on("zoom", zoomed);

var area = d3.area()
    .curve(d3.curveMonotoneX)
    .x(function(d) { return x(d.date); })
    .y0(height)
    .y1(function(d) { return y(d.price); });

var area2 = d3.area()
    .curve(d3.curveMonotoneX)
    .x(function(d) { return x2(d.date); })
    .y0(height2)
    .y1(function(d) { return y2(d.price); });

svg.append("defs").append("clipPath")
    .attr("id", "clip")
  .append("rect")
    .attr("width", width)
    .attr("height", height);

var focus = svg.append("g")
    .attr("class", "focus")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var context = svg.append("g")
    .attr("class", "context")
    .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

d3.csv("sp500.csv", type, function(error, data) {
  if (error) throw error;

  x.domain(d3.extent(data, function(d) { return d.date; }));
  y.domain([0, d3.max(data, function(d) { return d.price; })]);
  x2.domain(x.domain());
  y2.domain(y.domain());

  focus.append("path")
      .datum(data)
      .attr("class", "area")
      .attr("d", area);

  focus.append("g")
      .attr("class", "axis axis--x")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

  focus.append("g")
      .attr("class", "axis axis--y")
      .call(yAxis);

  context.append("path")
      .datum(data)
      .attr("class", "area")
      .attr("d", area2);

  context.append("g")
      .attr("class", "axis axis--x")
      .attr("transform", "translate(0," + height2 + ")")
      .call(xAxis2);

  context.append("g")
      .attr("class", "brush")
      .call(brush)
      .call(brush.move, x.range());

  svg.append("rect")
      .attr("class", "zoom")
      .attr("width", width)
      .attr("height", height)
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
      .call(zoom);
});

function brushed() {
  if (d3.event.sourceEvent && d3.event.sourceEvent.type === "zoom") return; // ignore brush-by-zoom
  var s = d3.event.selection || x2.range();
  x.domain(s.map(x2.invert, x2));
  focus.select(".area").attr("d", area);
  focus.select(".axis--x").call(xAxis);
  svg.select(".zoom").call(zoom.transform, d3.zoomIdentity
      .scale(width / (s[1] - s[0]))
      .translate(-s[0], 0));
}

function zoomed() {
  if (d3.event.sourceEvent && d3.event.sourceEvent.type === "brush") return; // ignore zoom-by-brush
  var t = d3.event.transform;
  x.domain(t.rescaleX(x2).domain());
  focus.select(".area").attr("d", area);
  focus.select(".axis--x").call(xAxis);
  context.select(".brush").call(brush.move, x.range().map(t.invertX, t));
}

function type(d) {
  d.date = parseDate(d.date);
  d.price = +d.price;
  return d;
}*/
var data = [
   {
      "Date":"2017-08-28T07:15:00Z",
      "Actual":4.915,
      "Consensous":null,
      "StandardDeviation":null,
      "StandardDeviationRatio":null,
      "TrueRange15Min":0.01045,
      "TrueRange4h":0.01045,
      "TrueRangeMedian15Min":0.00125,
      "TrueRangeMedian4h":0.00405,
      "TrueRangeMean15Min":0.00205,
      "TrueRangeMean4h":0.00491,
      "AverageTrueRange15Min":0.01046,
      "AverageTrueRange4h":0.00565,
      "VolatilityRatio15Min":0.99863,
      "VolatilityRatio4h":1.84847,
      "VolatilityRatioMedian15Min":1.07692,
      "VolatilityRatioMedian4h":1.24051,
      "VolatilityRatioMean15Min":1.18964,
      "VolatilityRatioMean4h":1.27462,
      "MarketDirection4h":"Bullish",
      "MarketDirection15Min":"Bullish"
   },
   {
      "Date":"2017-05-29T07:15:00Z",
      "Actual":4.884,
      "Consensous":null,
      "StandardDeviation":null,
      "StandardDeviationRatio":null,
      "TrueRange15Min":0.00241,
      "TrueRange4h":0.00291,
      "TrueRangeMedian15Min":0.0014,
      "TrueRangeMedian4h":0.00405,
      "TrueRangeMean15Min":0.00159,
      "TrueRangeMean4h":0.00464,
      "AverageTrueRange15Min":0.0098,
      "AverageTrueRange4h":0.00223,
      "VolatilityRatio15Min":0.24599,
      "VolatilityRatio4h":1.30299,
      "VolatilityRatioMedian15Min":1.34615,
      "VolatilityRatioMedian4h":1.24051,
      "VolatilityRatioMean15Min":1.26603,
      "VolatilityRatioMean4h":1.31364,
      "MarketDirection4h":"Bearish",
      "MarketDirection15Min":"Bullish"
   },
   {
      "Date":"2017-02-27T08:15:00Z",
      "Actual":4.912,
      "Consensous":null,
      "StandardDeviation":null,
      "StandardDeviationRatio":null,
      "TrueRange15Min":0.0011,
      "TrueRange4h":0.0025,
      "TrueRangeMedian15Min":0.0014,
      "TrueRangeMedian4h":0.00435,
      "TrueRangeMean15Min":0.00158,
      "TrueRangeMean4h":0.00487,
      "AverageTrueRange15Min":0.00074,
      "AverageTrueRange4h":0.0027,
      "VolatilityRatio15Min":1.48077,
      "VolatilityRatio4h":0.92593,
      "VolatilityRatioMedian15Min":1.07692,
      "VolatilityRatioMedian4h":1.24051,
      "VolatilityRatioMean15Min":1.23313,
      "VolatilityRatioMean4h":1.29027,
      "MarketDirection4h":"Bearish",
      "MarketDirection15Min":"Bullish"
   },
   {
      "Date":"2016-11-29T08:15:00Z",
      "Actual":4.918,
      "Consensous":null,
      "StandardDeviation":null,
      "StandardDeviationRatio":null,
      "TrueRange15Min":0.0007,
      "TrueRange4h":0.0041,
      "TrueRangeMedian15Min":0.0014,
      "TrueRangeMedian4h":0.00435,
      "TrueRangeMean15Min":0.00161,
      "TrueRangeMean4h":0.00499,
      "AverageTrueRange15Min":0.01004,
      "AverageTrueRange4h":0.00463,
      "VolatilityRatio15Min":0.34265,
      "VolatilityRatio4h":0.88526,
      "VolatilityRatioMedian15Min":1.32911,
      "VolatilityRatioMedian4h":1.24051,
      "VolatilityRatioMean15Min":1.29889,
      "VolatilityRatioMean4h":1.29671,
      "MarketDirection4h":"Bullish",
      "MarketDirection15Min":"Bullish"
   },
   {
      "Date":"2016-08-30T07:15:00Z",
      "Actual":4.903,
      "Consensous":null,
      "StandardDeviation":null,
      "StandardDeviationRatio":null,
      "TrueRange15Min":0.01233,
      "TrueRange4h":0.00378,
      "TrueRangeMedian15Min":0.00145,
      "TrueRangeMedian4h":0.0045,
      "TrueRangeMean15Min":0.00233,
      "TrueRangeMean4h":0.00532,
      "AverageTrueRange15Min":0.00884,
      "AverageTrueRange4h":0.00305,
      "VolatilityRatio15Min":0.42784,
      "VolatilityRatio4h":1.24051,
      "VolatilityRatioMedian15Min":1.33763,
      "VolatilityRatioMedian4h":1.51283,
      "VolatilityRatioMean15Min":1.32356,
      "VolatilityRatioMean4h":1.46028,
      "MarketDirection4h":"Bullish",
      "MarketDirection15Min":"Bearish"
   },
   {
      "Date":"2016-05-27T07:15:00Z",
      "Actual":4.878,
      "Consensous":null,
      "StandardDeviation":null,
      "StandardDeviationRatio":null,
      "TrueRange15Min":0.0007,
      "TrueRange4h":0.0015,
      "TrueRangeMedian15Min":0.00145,
      "TrueRangeMedian4h":0.00515,
      "TrueRangeMean15Min":0.00169,
      "TrueRangeMean4h":0.00556,
      "AverageTrueRange15Min":0.00074,
      "AverageTrueRange4h":0.00266,
      "VolatilityRatio15Min":0.95146,
      "VolatilityRatio4h":0.563,
      "VolatilityRatioMedian15Min":1.36898,
      "VolatilityRatioMedian4h":1.51283,
      "VolatilityRatioMean15Min":1.40029,
      "VolatilityRatioMean4h":1.46028,
      "MarketDirection4h":"Bullish",
      "MarketDirection15Min":"Bullish"
   },
   {
      "Date":"2016-02-26T08:15:00Z",
      "Actual":4.897,
      "Consensous":4.24,
      "StandardDeviation":0.00955,
      "StandardDeviationRatio":68.79581,
      "TrueRange15Min":0.0036,
      "TrueRange4h":0.0058,
      "TrueRangeMedian15Min":0.0016,
      "TrueRangeMedian4h":0.0054,
      "TrueRangeMean15Min":0.00184,
      "TrueRangeMean4h":0.00577,
      "AverageTrueRange15Min":0.0087,
      "AverageTrueRange4h":0.00313,
      "VolatilityRatio15Min":0.4139,
      "VolatilityRatio4h":1.85304,
      "VolatilityRatioMedian15Min":1.36898,
      "VolatilityRatioMedian4h":1.2268,
      "VolatilityRatioMean15Min":1.40792,
      "VolatilityRatioMean4h":1.4021,
      "MarketDirection4h":"Bearish",
      "MarketDirection15Min":"Bearish"
   },
   {
      "Date":"2015-08-25T07:15:00Z",
      "Actual":4.24,
      "Consensous":4.24,
      "StandardDeviation":0.00963,
      "StandardDeviationRatio":0,
      "TrueRange15Min":0.0029,
      "TrueRange4h":0.0109,
      "TrueRangeMedian15Min":0.0016,
      "TrueRangeMedian4h":0.0054,
      "TrueRangeMean15Min":0.00176,
      "TrueRangeMean4h":0.00601,
      "AverageTrueRange15Min":0.00199,
      "AverageTrueRange4h":0.00971,
      "VolatilityRatio15Min":1.46043,
      "VolatilityRatio4h":1.12206,
      "VolatilityRatioMedian15Min":1.43275,
      "VolatilityRatioMedian4h":1.30435,
      "VolatilityRatioMean15Min":1.4942,
      "VolatilityRatioMean4h":1.44577,
      "MarketDirection4h":"Bullish",
      "MarketDirection15Min":"Bearish"
   },
   {
      "Date":"2015-05-26T07:15:00Z",
      "Actual":4.225,
      "Consensous":4.21,
      "StandardDeviation":0.00638,
      "StandardDeviationRatio":2.3511,
      "TrueRange15Min":0.0026,
      "TrueRange4h":0.0146,
      "TrueRangeMedian15Min":0.0015,
      "TrueRangeMedian4h":0.00435,
      "TrueRangeMean15Min":0.00168,
      "TrueRangeMean4h":0.00565,
      "AverageTrueRange15Min":0.00161,
      "AverageTrueRange4h":0.00699,
      "VolatilityRatio15Min":1.61062,
      "VolatilityRatio4h":0.30889,
      "VolatilityRatioMedian15Min":1.39181,
      "VolatilityRatioMedian4h":1.03858,
      "VolatilityRatioMean15Min":1.48524,
      "VolatilityRatioMean4h":1.40722,
      "MarketDirection4h":"Bearish",
      "MarketDirection15Min":"Bearish"
   },
   {
      "Date":"2015-02-24T08:15:00Z",
      "Actual":4.231,
      "Consensous":null,
      "StandardDeviation":null,
      "StandardDeviationRatio":null,
      "TrueRange15Min":0.0014,
      "TrueRange4h":0.0039,
      "TrueRangeMedian15Min":0.00145,
      "TrueRangeMedian4h":0.00475,
      "TrueRangeMean15Min":0.00161,
      "TrueRangeMean4h":0.00565,
      "AverageTrueRange15Min":0.00095,
      "AverageTrueRange4h":0.0042,
      "VolatilityRatio15Min":1.47368,
      "VolatilityRatio4h":0.92857,
      "VolatilityRatioMedian15Min":1.36898,
      "VolatilityRatioMedian4h":1.72131,
      "VolatilityRatioMean15Min":1.4862,
      "VolatilityRatioMean4h":1.57301,
      "MarketDirection4h":"Bearish",
      "MarketDirection15Min":"Bearish"
   },
   {
      "Date":"2014-11-24T08:15:00Z",
      "Actual":4.227,
      "Consensous":4.22,
      "StandardDeviation":0.00376,
      "StandardDeviationRatio":1.8617,
      "TrueRange15Min":0.0031,
      "TrueRange4h":0.0042,
      "TrueRangeMedian15Min":0.00105,
      "TrueRangeMedian4h":0.00435,
      "TrueRangeMean15Min":0.00138,
      "TrueRangeMean4h":0.00483,
      "AverageTrueRange15Min":0.00085,
      "AverageTrueRange4h":0.00243,
      "VolatilityRatio15Min":3.64706,
      "VolatilityRatio4h":1.72603,
      "VolatilityRatioMedian15Min":1.07692,
      "VolatilityRatioMedian4h":1.72131,
      "VolatilityRatioMean15Min":1.15805,
      "VolatilityRatioMean4h":1.6526,
      "MarketDirection4h":"Bearish",
      "MarketDirection15Min":"Bullish"
   },
   {
      "Date":"2014-08-28T07:15:00Z",
      "Actual":4.196,
      "Consensous":4.207,
      "StandardDeviation":0.02083,
      "StandardDeviationRatio":-0.52808,
      "TrueRange15Min":0.0007,
      "TrueRange4h":0.0045,
      "TrueRangeMedian15Min":0.00105,
      "TrueRangeMedian4h":0.0045,
      "TrueRangeMean15Min":0.00115,
      "TrueRangeMean4h":0.0051,
      "AverageTrueRange15Min":0.00065,
      "AverageTrueRange4h":0.00261,
      "VolatilityRatio15Min":1.07692,
      "VolatilityRatio4h":1.72131,
      "VolatilityRatioMedian15Min":1.34615,
      "VolatilityRatioMedian4h":1.8871,
      "VolatilityRatioMean15Min":1.22103,
      "VolatilityRatioMean4h":1.68601,
      "MarketDirection4h":"Bullish",
      "MarketDirection15Min":"Bearish"
   },
   {
      "Date":"2014-05-27T07:15:00Z",
      "Actual":4.192,
      "Consensous":4.193,
      "StandardDeviation":0.0203,
      "StandardDeviationRatio":-0.04926,
      "TrueRange15Min":0.0011,
      "TrueRange4h":0.0022,
      "TrueRangeMedian15Min":0.00125,
      "TrueRangeMedian4h":0.00515,
      "TrueRangeMean15Min":0.0014,
      "TrueRangeMean4h":0.00545,
      "AverageTrueRange15Min":0.00074,
      "AverageTrueRange4h":0.00191,
      "VolatilityRatio15Min":1.48077,
      "VolatilityRatio4h":1.14925,
      "VolatilityRatioMedian15Min":0.98718,
      "VolatilityRatioMedian4h":1.8871,
      "VolatilityRatioMean15Min":1.12231,
      "VolatilityRatioMean4h":1.57123,
      "MarketDirection4h":"Bullish",
      "MarketDirection15Min":"Bullish"
   },
   {
      "Date":"2014-02-27T08:15:00Z",
      "Actual":4.189,
      "Consensous":4.191,
      "StandardDeviation":0.021,
      "StandardDeviationRatio":-0.09524,
      "TrueRange15Min":0.001,
      "TrueRange4h":0.0045,
      "TrueRangeMedian15Min":0.00145,
      "TrueRangeMedian4h":0.00515,
      "TrueRangeMean15Min":0.00147,
      "TrueRangeMean4h":0.00568,
      "AverageTrueRange15Min":0.00102,
      "AverageTrueRange4h":0.00345,
      "VolatilityRatio15Min":0.97902,
      "VolatilityRatio4h":1.30435,
      "VolatilityRatioMedian15Min":1.32911,
      "VolatilityRatioMedian4h":1.8871,
      "VolatilityRatioMean15Min":1.19233,
      "VolatilityRatioMean4h":1.50672,
      "MarketDirection4h":"Bearish",
      "MarketDirection15Min":"Bearish"
   },
   {
      "Date":"2013-11-25T08:15:00Z",
      "Actual":4.196,
      "Consensous":4.197,
      "StandardDeviation":0.01923,
      "StandardDeviationRatio":-0.052,
      "TrueRange15Min":0.001,
      "TrueRange4h":0.0039,
      "TrueRangeMedian15Min":0.00145,
      "TrueRangeMedian4h":0.0062,
      "TrueRangeMean15Min":0.00152,
      "TrueRangeMean4h":0.00627,
      "AverageTrueRange15Min":0.00074,
      "AverageTrueRange4h":0.00207,
      "VolatilityRatio15Min":1.34615,
      "VolatilityRatio4h":1.8871,
      "VolatilityRatioMedian15Min":1.15815,
      "VolatilityRatioMedian4h":1.88837,
      "VolatilityRatioMean15Min":1.15388,
      "VolatilityRatioMean4h":1.68376,
      "MarketDirection4h":"Bullish",
      "MarketDirection15Min":"Bearish"
   },
   {
      "Date":"2013-08-29T07:15:00Z",
      "Actual":4.166,
      "Consensous":4.17,
      "StandardDeviation":0.01462,
      "StandardDeviationRatio":-0.2736,
      "TrueRange15Min":0.0014,
      "TrueRange4h":0.0097,
      "TrueRangeMedian15Min":0.0016,
      "TrueRangeMedian4h":0.0071,
      "TrueRangeMean15Min":0.0017,
      "TrueRangeMean4h":0.00688,
      "AverageTrueRange15Min":0.00154,
      "AverageTrueRange4h":0.00441,
      "VolatilityRatio15Min":0.90741,
      "VolatilityRatio4h":2.20097,
      "VolatilityRatioMedian15Min":1.36046,
      "VolatilityRatioMedian4h":1.14655,
      "VolatilityRatioMean15Min":1.30203,
      "VolatilityRatioMean4h":1.47287,
      "MarketDirection4h":"Bullish",
      "MarketDirection15Min":"Bearish"
   },
   {
      "Date":"2013-05-28T07:15:00Z",
      "Actual":4.152,
      "Consensous":4.102,
      "StandardDeviation":0.01134,
      "StandardDeviationRatio":4.40917,
      "TrueRange15Min":0.0017,
      "TrueRange4h":0.0058,
      "TrueRangeMedian15Min":0.00175,
      "TrueRangeMedian4h":0.0062,
      "TrueRangeMean15Min":0.00177,
      "TrueRangeMean4h":0.0061,
      "AverageTrueRange15Min":0.00122,
      "AverageTrueRange4h":0.00307,
      "VolatilityRatio15Min":1.39181,
      "VolatilityRatio4h":1.88837,
      "VolatilityRatioMedian15Min":1.41456,
      "VolatilityRatioMedian4h":1.03858,
      "VolatilityRatioMean15Min":1.35281,
      "VolatilityRatioMean4h":1.30291,
      "MarketDirection4h":"Bearish",
      "MarketDirection15Min":"Bearish"
   },
   {
      "Date":"2013-02-26T08:15:00Z",
      "Actual":4.116,
      "Consensous":4.107,
      "StandardDeviation":0.001,
      "StandardDeviationRatio":9,
      "TrueRange15Min":0.0022,
      "TrueRange4h":0.0066,
      "TrueRangeMedian15Min":0.00195,
      "TrueRangeMedian4h":0.0071,
      "TrueRangeMean15Min":0.00187,
      "TrueRangeMean4h":0.00673,
      "AverageTrueRange15Min":0.00223,
      "AverageTrueRange4h":0.01147,
      "VolatilityRatio15Min":0.98718,
      "VolatilityRatio4h":0.57534,
      "VolatilityRatioMedian15Min":1.54703,
      "VolatilityRatioMedian4h":1.14655,
      "VolatilityRatioMean15Min":1.50453,
      "VolatilityRatioMean4h":1.59927,
      "MarketDirection4h":"Bearish",
      "MarketDirection15Min":"Bullish"
   },
   {
      "Date":"2012-11-26T08:15:00Z",
      "Actual":4.122,
      "Consensous":4.088,
      "StandardDeviation":0.011,
      "StandardDeviationRatio":3.09091,
      "TrueRange15Min":0.0015,
      "TrueRange4h":0.0036,
      "TrueRangeMedian15Min":0.0018,
      "TrueRangeMedian4h":0.0076,
      "TrueRangeMean15Min":0.0018,
      "TrueRangeMean4h":0.00676,
      "AverageTrueRange15Min":0.00113,
      "AverageTrueRange4h":0.00367,
      "VolatilityRatio15Min":1.32911,
      "VolatilityRatio4h":0.98182,
      "VolatilityRatioMedian15Min":1.59406,
      "VolatilityRatioMedian4h":1.60185,
      "VolatilityRatioMean15Min":1.563,
      "VolatilityRatioMean4h":1.75364,
      "MarketDirection4h":"Bullish",
      "MarketDirection15Min":"Bullish"
   },
   {
      "Date":"2012-08-28T07:15:09Z",
      "Actual":4.072,
      "Consensous":4.04,
      "StandardDeviation":0.005,
      "StandardDeviationRatio":6.4,
      "TrueRange15Min":0.0013,
      "TrueRange4h":0.008,
      "TrueRangeMedian15Min":0.00195,
      "TrueRangeMedian4h":0.0078,
      "TrueRangeMean15Min":0.00188,
      "TrueRangeMean4h":0.00755,
      "AverageTrueRange15Min":null,
      "AverageTrueRange4h":0.00289,
      "VolatilityRatio15Min":null,
      "VolatilityRatio4h":2.77228,
      "VolatilityRatioMedian15Min":null,
      "VolatilityRatioMedian4h":1.14655,
      "VolatilityRatioMean15Min":null,
      "VolatilityRatioMean4h":1.41409,
      "MarketDirection4h":"Bearish",
      "MarketDirection15Min":"Bearish"
   },
   {
      "Date":"2012-05-25T07:15:00Z",
      "Actual":4.049,
      "Consensous":null,
      "StandardDeviation":null,
      "StandardDeviationRatio":null,
      "TrueRange15Min":0.0021,
      "TrueRange4h":0.0076,
      "TrueRangeMedian15Min":0.0021,
      "TrueRangeMedian4h":0.0076,
      "TrueRangeMean15Min":0.00207,
      "TrueRangeMean4h":0.0074,
      "AverageTrueRange15Min":0.0014,
      "AverageTrueRange4h":0.00663,
      "VolatilityRatio15Min":1.5,
      "VolatilityRatio4h":1.14655,
      "VolatilityRatioMedian15Min":1.5945,
      "VolatilityRatioMedian4h":1.54786,
      "VolatilityRatioMean15Min":1.5945,
      "VolatilityRatioMean4h":1.54786,
      "MarketDirection4h":"Bearish",
      "MarketDirection15Min":"Bearish"
   },
   {
      "Date":"2012-02-28T07:15:00Z",
      "Actual":4.044,
      "Consensous":null,
      "StandardDeviation":null,
      "StandardDeviationRatio":null,
      "TrueRange15Min":0.0018,
      "TrueRange4h":0.005,
      "TrueRangeMedian15Min":0.00205,
      "TrueRangeMedian4h":0.0073,
      "TrueRangeMean15Min":0.00205,
      "TrueRangeMean4h":0.0073,
      "AverageTrueRange15Min":0.00113,
      "AverageTrueRange4h":0.00481,
      "VolatilityRatio15Min":1.59494,
      "VolatilityRatio4h":1.03858,
      "VolatilityRatioMedian15Min":1.59406,
      "VolatilityRatioMedian4h":2.05714,
      "VolatilityRatioMean15Min":1.59406,
      "VolatilityRatioMean4h":2.05714,
      "MarketDirection4h":"Bearish",
      "MarketDirection15Min":"Bullish"
   },
   {
      "Date":"2011-12-12T07:15:00Z",
      "Actual":4.047,
      "Consensous":null,
      "StandardDeviation":null,
      "StandardDeviationRatio":null,
      "TrueRange15Min":0.0023,
      "TrueRange4h":0.0096,
      "TrueRangeMedian15Min":0.0023,
      "TrueRangeMedian4h":0.0096,
      "TrueRangeMean15Min":0.0023,
      "TrueRangeMean4h":0.0096,
      "AverageTrueRange15Min":0.00144,
      "AverageTrueRange4h":0.00467,
      "VolatilityRatio15Min":1.59406,
      "VolatilityRatio4h":2.05714,
      "VolatilityRatioMedian15Min":null,
      "VolatilityRatioMedian4h":null,
      "VolatilityRatioMean15Min":null,
      "VolatilityRatioMean4h":null,
      "MarketDirection4h":"Bullish",
      "MarketDirection15Min":"Bullish"
   },
   {
      "Date":"2011-05-26T07:15:00Z",
      "Actual":4.11,
      "Consensous":4.1,
      "StandardDeviation":0.04821,
      "StandardDeviationRatio":0.20743,
      "TrueRange15Min":null,
      "TrueRange4h":null,
      "TrueRangeMedian15Min":null,
      "TrueRangeMedian4h":null,
      "TrueRangeMean15Min":null,
      "TrueRangeMean4h":null,
      "AverageTrueRange15Min":null,
      "AverageTrueRange4h":null,
      "VolatilityRatio15Min":null,
      "VolatilityRatio4h":null,
      "VolatilityRatioMedian15Min":null,
      "VolatilityRatioMedian4h":null,
      "VolatilityRatioMean15Min":null,
      "VolatilityRatioMean4h":null,
      "MarketDirection4h":null,
      "MarketDirection15Min":null
   },
   {
      "Date":"2011-02-24T08:15:00Z",
      "Actual":4.1,
      "Consensous":4.1,
      "StandardDeviation":0.04821,
      "StandardDeviationRatio":0,
      "TrueRange15Min":null,
      "TrueRange4h":null,
      "TrueRangeMedian15Min":null,
      "TrueRangeMedian4h":null,
      "TrueRangeMean15Min":null,
      "TrueRangeMean4h":null,
      "AverageTrueRange15Min":null,
      "AverageTrueRange4h":null,
      "VolatilityRatio15Min":null,
      "VolatilityRatio4h":null,
      "VolatilityRatioMedian15Min":null,
      "VolatilityRatioMedian4h":null,
      "VolatilityRatioMean15Min":null,
      "VolatilityRatioMean4h":null,
      "MarketDirection4h":null,
      "MarketDirection15Min":null
   }
]