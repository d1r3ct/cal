(function(factory) {

  define(function() {
    return factory(window, document);
  });

})(function(window, document) {
  var mockF = {};

  mockF = {
    firstLev: [{
  "id": 1,
  "title": "Alfa-Bank-21 USD 7.3/4",
  "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
  "symbol": "A21#1_VTB_PERP_USD",
  "expirationDate": "2021-04-28T00:00:00.000000+03:00",
  "initAsk": "1255,2",
  "currentAsk": "1155,2",
  "couponYield": "0,21",
  "currency": "USD"
},
{
  "id": 2,
  "title": "VEB-22 USD 6.025",
  "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
  "symbol": "V22#1_VEB_2022_USD",
  "expirationDate": "2022-07-05T00:00:00.000000+03:00",
  "initAsk": "1360",
  "currentAsk": "1090",
  "couponYield": "0,16",
  "currency": "USD"
},
{
  "id": 3,
  "title": "VTB Perpetual USD",
  "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
  "symbol": "VT9#1_VTB_PERP_USD",
  "expirationDate": "2049-12-29T00:00:00.000000+03:00",
  "initAsk": "1150",
  "currentAsk": "1130",
  "couponYield": "0,26",
  "currency": "USD"
},
{
  "id": 4,
  "title": "Gazprombank Perpetual",
  "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
  "symbol": "GB9#1_GAZPROMBANK_PERP_USD",
  "expirationDate": "2049-12-29T00:00:00.000000+03:00",
  "initAsk": "1330",
  "currentAsk": "1047",
  "couponYield": "0,21",
  "currency": "USD"
},
{
  "id": 5,
  "title": "Sberbank 23 USD 5.25",
  "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
  "symbol": "SB3#1_SBERBANK_2023_USD",
  "expirationDate": "2023-05-23T00:00:00.000000+03:00",
  "initAsk": "1999",
  "currentAsk": "1050",
  "couponYield": "0,14",
  "currency": "USD"
},
{
  "id": 6,
  "title": "HSBC Perpetual USD 5.625",
  "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
  "symbol": "HU9#1_HSBC_PERP_USD",
  "expirationDate": "2049-12-29T00:00:00.000000+03:00",
  "initAsk": "1799",
  "currentAsk": "1050",
  "couponYield": "0,15",
  "currency": "USD"
},
{
  "id": 7,
  "title": "Standard Chartered USD 5.2",
  "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
  "symbol": "CU4#1_STANDARD_CHART_2024_USD",
  "expirationDate": "2024-01-26T00:00:00.000000+03:00",
  "initAsk": "1500",
  "currentAsk": "1095",
  "couponYield": "0,14",
  "currency": "USD"
}],

    secondLev: [
      {
        "id": 8,
        "title": "Alfa-Bank-21 USD 7/4",
        "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
        "symbol": "VT9#2_VTB_PERP_USD",
        "expirationDate": "2020-04-28T00:00:00.000000+03:00",
        "initAsk": "2152,8",
        "currentAsk": "2261.00000000",
        "couponYield": "22",
        "currency": "USD"
      },
      {
        "id": 9,
        "title": "VEB-22 USD 6.025",
        "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
        "symbol": "VT2#2_VTB_2020_USD",
        "expirationDate": "2021-04-28T00:00:00.000000+03:00",
        "initAsk": "2152,8",
        "currentAsk": "2261.00000000",
        "couponYield": "23,27",
        "currency": "USD"
      },
      {
        "id": 10,
        "title": "Gazprom 2-22 USD/5.3",
        "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
        "symbol": "V22#2_VEB_2022_USD",
        "expirationDate": "2019-04-28T00:00:00.000000+03:00",
        "initAsk": "2152,8",
        "currentAsk": "2261.00000000",
        "couponYield": "23,27",
        "currency": "USD"
      },
      {
        "id": 11,
        "title": "VEB-22 USD 6.025",
        "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
        "symbol": "VT2#2_VTB_2020_USD",
        "expirationDate": "2021-04-28T00:00:00.000000+03:00",
        "initAsk": "2152,8",
        "currentAsk": "2261.00000000",
        "couponYield": "23,27",
        "currency": "USD"
      },
      {
        "id": 12,
        "title": "VEB-22 USD 6.025",
        "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
        "symbol": "VT2#2_VTB_2020_USD",
        "expirationDate": "2021-04-28T00:00:00.000000+03:00",
        "initAsk": "2152,8",
        "currentAsk": "2261.00000000",
        "couponYield": "23,27",
        "currency": "USD"
      },
      {
        "id": 13,
        "title": "VEB-22 USD 6.025",
        "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
        "symbol": "VT2#2_VTB_2020_USD",
        "expirationDate": "2021-04-28T00:00:00.000000+03:00",
        "initAsk": "2152,8",
        "currentAsk": "2261.00000000",
        "couponYield": "23,27",
        "currency": "USD"
      },
      {
        "id": 14,
        "title": "VEB-22 USD 6.025",
        "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
        "symbol": "VT2#2_VTB_2020_USD",
        "expirationDate": "2021-04-28T00:00:00.000000+03:00",
        "initAsk": "2152,8",
        "currentAsk": "2261.00000000",
        "couponYield": "23,27",
        "currency": "USD"
      },
      {
        "id": 15,
        "title": "VEB-22 USD 6.025",
        "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
        "symbol": "VT2#2_VTB_2020_USD",
        "expirationDate": "2021-04-28T00:00:00.000000+03:00",
        "initAsk": "2152,8",
        "currentAsk": "2261.00000000",
        "couponYield": "23,27",
        "currency": "USD"
      },
      {
        "id": 16,
        "title": "VEB-22 USD 6.025",
        "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
        "symbol": "VT2#2_VTB_2020_USD",
        "expirationDate": "2021-04-28T00:00:00.000000+03:00",
        "initAsk": "2152,8",
        "currentAsk": "2261.00000000",
        "couponYield": "23,27",
        "currency": "USD"
      },
      {
        "id": 17,
        "title": "VEB-22 USD 6.025",
        "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
        "symbol": "VT2#2_VTB_2020_USD",
        "expirationDate": "2021-04-28T00:00:00.000000+03:00",
        "initAsk": "2152,8",
        "currentAsk": "2261.00000000",
        "couponYield": "23,27",
        "currency": "USD"
      },
      {
        "id": 18,
        "title": "Gazprom 2-22 USD/5.3",
        "companyLogo": "path/to/img.png",
        "symbol": "B21",
        "expirationDate": "2021-04-28T00:00:00.000000+03:00",
        "initAsk": "2152,8",
        "currentAsk": "2261.00000000",
        "couponYield": "23,27",
        "currency": "EUR"
      }
    ],

    fifthLev: [
      {
        "id": 19,
        "title": "Alfa-Bank-21 USD 7/4",
        "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
        "symbol": "VT9#5_VTB_PERP_USD",
        "expirationDate": "2020-04-28T00:00:00.000000+03:00",
        "initAsk": "2152,8",
        "currentAsk": "2261.00000000",
        "couponYield": "22",
        "currency": "USD"
      },
      {
        "id": 20,
        "title": "VEB-22 USD 6.025",
        "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
        "symbol": "VT2#5_VTB_2020_USD",
        "expirationDate": "2021-04-28T00:00:00.000000+03:00",
        "initAsk": "2152,8",
        "currentAsk": "2261.00000000",
        "couponYield": "23,27",
        "currency": "USD"
      },
      {
        "id": 21,
        "title": "Gazprom 2-22 USD/5.3",
        "companyLogo": "http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",
        "symbol": "V22#5_VEB_2022_USD",
        "expirationDate": "2019-04-15T00:00:00.000000+03:00",
        "initAsk": "2152,8",
        "currentAsk": "2261.00000000",
        "couponYield": "23,27",
        "currency": "USD"
      },
      {
        "id": 22,
        "title": "Gazprom 2-22 USD/5.3",
        "companyLogo": "path/to/img.png",
        "symbol": "B21",
        "expirationDate": "2021-04-28T00:00:00.000000+03:00",
        "initAsk": "2152,8",
        "currentAsk": "2261.00000000",
        "couponYield": "23,27",
        "currency": "EUR"
      }
    ],

    getInitData: function(leverage) {
      switch(leverage) {
        case 5:
          return this.fifthLev;
        case 2:
          return this.secondLev;
        case 1:
        default:
          return this.firstLev;
      }
    }

  };

  return mockF;
});

