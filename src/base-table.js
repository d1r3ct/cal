define(['underscore'], function() {
  'use strict';

  var SortOrder = {
    'asc'   : 0,
    'desc'  : 1
  };

  function BaseTable($view, options) {
    var state = {},
    columns = [];

    var classNames = _getClassNames(options.baseClass);

    var tHead = $('thead', $view);
    if (tHead.length !== 0) {
      columns = _getColumnsFromHeader(tHead);
    }

    var tBody = $('tbody', $view);
    if (tBody.length === 0) {
      tBody = $(document.createElement('tbody')).appendTo($view);
    }

    state.thead = tHead[0];
    state.tbody = tBody[0];

    var sortParam = {
      'key': null,
      'order': SortOrder.desc
    }

    _.extend(this, state, options, {
      'view': $view,
      'columns': columns,
      'classNames': classNames,
      'sortParam': sortParam
    });
  }

  /**
   * Sorts "rows" array in the state.
   *
   * @param  {string} column
   * @return {void}
   */
  BaseTable.prototype.sortByColumn = function(column) {
    var sortParam = this.sortParam;
    var sCallback;

    if (sortParam.key && sortParam.key === column) {
      sortParam.order = sortParam.order ? SortOrder.asc : SortOrder.desc;
    } else {
      sortParam.order = SortOrder.asc;
    }

    sortParam.key = column;

    if (typeof this.rows[0].cells[column] === 'object') {
      sCallback = function(a, b) {
        return sortParam.order ?
          b.cells[column].sortData - a.cells[column].sortData :
          a.cells[column].sortData - b.cells[column].sortData;
      }
    } else {
      sCallback = function(a, b) {
        return sortParam.order ?
          b.cells[column] - a.cells[column] :
          a.cells[column] - b.cells[column];
      }
    }

    // sort an array in place
    this.rows.sort(sCallback);
  }

  /**
   * Builds a single tr with its td children
   *
   * @param  {Object}   rowData       Bond response data
   * @param  {number}   rowIdx        The row's index in teh table
   * @param  {function} calcCellValue Callback used to calculate a value for each cell
   * @param  {[type]}   options       Extra values
   * @return {[type]}
   */
  BaseTable.prototype.createTr = function(rowData, rowIdx, calcCellValue, options) {
    var row = {},
        self = this;

    var tr = document.createElement('tr');

    // store the row position index on a DOM node
    tr._AF_rowIndex = rowIdx;
    tr._AF_rowId = rowData.id;

    tr.classList.add(self.classNames.bodyRow);

    row.nTr = tr;
    row.cells = {};
    row.id = rowData.id;

    var bondWorthDynamics = rowData.currentAsk > rowData.initAsk;

    // direct mapping of the value in the state
    _.each(self.columns, function(col, colIdx) {
      var cellVal;
      var nTd = document.createElement('td');

      nTd.classList.add(self.classNames.bodyCell);

      nTd._AF_cellIndex = {
        'row': rowIdx,
        'column': colIdx
      }

      cellVal = calcCellValue(col.key, rowData, row.cells, options);

      /* By returning an object with an html property, we can store additional
       * data in the state */
      if (typeof cellVal === 'object') {
        nTd.innerHTML = cellVal.html;
      } else {
        nTd.innerHTML = cellVal;
      }

      /*
       * if the column has the dynamics growth, we will add an additional class to it
       */
      if (col.dynamics) {
        nTd.classList.add(bondWorthDynamics ? self.classNames.dynamicsUp : self.classNames.dynamicsDown);
      }

      row.cells[col.key] = cellVal;

      tr.appendChild(nTd);
    })

    return row;
  }

  /**
   * The function traverses the table header and collects the columns
   *
   * @param  {HTMLElement} tHead
   * @return {Object}      The object with columns data
   */
  function _getColumnsFromHeader(tHead) {
    var headerRow = $('tr', tHead);
    var columns = [];

    $.each(headerRow.children('th'), function(idx) {
      columns.push({
        'id'      : idx,
        'key'     : $(this).data('columnKey'),
        'sortable': $(this).data('sortable'),
        'dynamics' : $(this).data('dynamics')
      });
    });

    return columns;
  }

  function _getClassNames(baseClass) {
    return {
      'tableEmpty'  : baseClass + '--empty',
      'headRow'     : baseClass + '__head-row',
      'headCell'    : baseClass + '__head-cell',
      'body'        : baseClass + '__body',
      'bodyRow'     : baseClass + '__body-row',
      'bodyCell'    : baseClass + '__body-cell',
      'detailTrig'  : baseClass + '__detail-trig',
      'detailView'  : baseClass + '__detail-view',
      'dynamicsUp'  : baseClass + '__dynamic--up',
      'dynamicsDown': baseClass + '__dynamic--down',
      'placeholder' : baseClass + '__placeholder'
    }
  }

  /**
   * Insert the table rows into the DOM
   * @param {object} state
   */
  function _render(state) {
    /*
     * Render table body
     */
    var tableBody = $(state.tbody);
    tableBody.children().remove();
    var rowsToRender = [];

    _.each(state.rows, function(item, idx) {
      rowsToRender.push(item.nTr);
    });

    tableBody.append($(rowsToRender));
  }

  return BaseTable;
})
