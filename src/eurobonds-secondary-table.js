define(['base-table', 'underscore', 'jquery-selectric'], function(BaseTable) {
  'use strict';

  // it only does '%s', and return '' when arguments are undefined
  var sprintf = function (str) {
      var args = arguments,
          flag = true,
          i = 1;

      str = str.replace(/%s/g, function () {
          var arg = args[i++];

          if (typeof arg === 'undefined') {
              flag = false;
              return '';
          }
          return arg;
      });
      return flag ? str : '';
  };

  function roundToTwoDecimal(x) {
    return Math.round(x * 100) / 100;
  }

  function EurobondsCompTable($view, options) {
    var self = this,
        rows = [],
        selectedRows = [];

    BaseTable.call(this, $view, options);

    // the table is hidden by default, we will open it when adding new rows
    self.view.addClass('hidden');

    _.extend(this, {
      'rows': rows,
      'selectedRows': selectedRows
    });

    this.appendRow = this.appendRow.bind(this);
  }

  EurobondsCompTable.prototype = Object.create(BaseTable.prototype);
  EurobondsCompTable.prototype.constructor = EurobondsCompTable;

  EurobondsCompTable.prototype.getState = function() {
    return this;
  }

  EurobondsCompTable.prototype.setState = function(newState) {
    _.extend(this, newState);
  };

  EurobondsCompTable.prototype.getRowData = function(rowId) {
    return _.find(this.responseData, function(i) {
      return i.id === rowId;
    })
  }

  EurobondsCompTable.prototype.getExistingRow = function(rowId) {
    return _.find(this.rows, function(i) {
      return i.id === rowId;
    })
  }

  EurobondsCompTable.prototype.appendRow = function(primaryRowState, quantity) {
    quantity = quantity ? Number(quantity) : 1;

    // select the row data from the response
    var rowData = this.getRowData(primaryRowState.id);

    var rowIdx = this.rows.length;

    var options = {
      'quantity': quantity,
      'leverage': this.leverage,
      'rowState': primaryRowState.cells
    };

    var row = BaseTable.prototype.createTr.call(this, rowData, rowIdx, calculateCellValue, options);

    this.rows.push(row);

    this.render();
  }

  EurobondsCompTable.prototype.updateAmount = function(rowData, quantity, increment) {
    var self = this;

    // get the row from the state
    var rowData = self.getRowData(rowData.id);
    var row = self.getExistingRow(rowData.id);
    var newAmount = increment ? row.cells.quantity.value + Number(quantity) : Number(quantity);

    // I don't really have to pass valid IDx here though.
    var updatedRow = BaseTable.prototype.createTr.call(self, rowData, rowData.id, calculateCellValue, {
      'quantity': newAmount,
      'leverage': self.leverage,
      'rowState': row.cells
    });

    // we found and created a row, now I update the state and then simply re-render
    // put udpatedRow on a position of "rowId"
    // Can it be that I updating the state inside the method, but this function's
    // scope is has this set to that particular state? No, because I've a getter.
    self.updateRow(updatedRow);

    self.render();
  }

  EurobondsCompTable.prototype.updateRow = function(updatedRow) {
    var state = this.getState();

    var rows = _.reduce(this.rows, function(acc, iRow, idx) {

      if (iRow.idx === updatedRow.id) {
        acc[idx] = updatedRow;
      } else {
        acc[idx] = iRow;
      }

      return acc;
    }, []);

    state = _.extend({}, state, {
      'rows': rows
    });

    this.setState(state);
  }

  EurobondsCompTable.prototype.toggleSelectRow = function(row) {
    var rowId = row._AF_rowId;
    var stateIdPos = this.selectedRows.indexOf(rowId);

    if (stateIdPos != -1) {
      this.selectedRows.splice(stateIdPos, 1);
      row.classList.remove('active');
    } else {
      this.selectedRows.push(rowId);
      row.classList.add('active');
    }

    return this.selectedRows.length;
  }

  /**
   * Deletes the rows that are currently in "selectedRows" state and re-renders
   * the table
   * @return {void}
   */
  EurobondsCompTable.prototype.deleteRows = function() {
    var self = this;

    this.rows = _.filter(this.rows, function(row) {
      return !_.contains(self.selectedRows, row.nTr._AF_rowId);
    });

    // also clear the selected rows state
    this.selectedRows = [];

    this.render();

    return this.rows.length;
  }

  EurobondsCompTable.prototype.render = function() {
    /*
     * Render table body
     */
    var tableBody = $(this.tbody);
    tableBody.children().remove();
    var rowsToRender = [];

    _.each(this.rows, function(item, idx) {
      rowsToRender.push(item.nTr);
    });

    /*
     * Add a summary row to the table
     *
     * This call should be somewhere outside the render, so I can use the render
     * from the BaseTable.
     */
    var summaryTr = buildSummaryRow(this.rows, this.summaryTitle);
    rowsToRender = rowsToRender.concat(summaryTr);

    tableBody.append($(rowsToRender));
  }

  /**
   * The function traverses the table header and collects the columns
   *
   * @param  {HTMLElement} tHead
   * @return {Object}      The object with columns data
   */
  function _getColumnsFromHeader(tHead) {
    var headerRow = $('tr', tHead);
    var columns = [];

    $.each(headerRow.children('th'), function(idx) {
      columns.push({
        'id': idx,
        'key': $(this).data('columnKey'),
        'sortable': Boolean($(this).data('sortable'))
      });
    });

    return columns;
  }


  /**
   * The function calculates and returns the cell values
   *
   * @param  {string} columnName
   * @param  {Object} bondsData
   * @param  {number} existingCells
   * @param  {number} options
   * @return {any}
   */
  function calculateCellValue(columnName, bondsData, existingCells, options) {
    switch (columnName) {
      case 'company':
        return {
          'html': options.rowState.company.html
        };

      case 'couponYield':
      case 'maturityYield':
        return roundToTwoDecimal(options.rowState[columnName] * options.quantity);

      case 'provision':
        return roundToTwoDecimal( (bondsData.currentAsk / options.leverage) * options.quantity );

      case 'maturityYieldPercent':
      case 'annualYield':
        return roundToTwoDecimal(options.rowState[columnName]);

      case 'quantity':
        // this should return an input.
        return {
          'value': roundToTwoDecimal(options.quantity),
          'html' : sprintf('<input class="computation-quantity" type="text" value="%s"/>', options.quantity)
        }

      default:
        return Number(options.rowState[columnName]) * quantity;
    }
  }

  // function this.render({}

  function buildSummaryRow(rows, title) {
    title = title || 'Summary';

    var defaultSummary = {
      'company': title,
      'quantity': 0,
      'couponYield': 0,
      'provision': 0,
      'maturityYield': 0,
      'maturityYieldPercent': 0,
      'annualYield': 0
    }

    // loop through all the existing rows
    var sRow = _.reduce(rows, function(acc, item, idx, rows) {
      acc['company'] = title;
      acc['quantity'] += Number(item.cells.quantity.value);
      acc['couponYield'] += item.cells.couponYield;
      acc['provision'] += roundToTwoDecimal(item.cells.provision);
      acc['maturityYield'] += item.cells.maturityYield;
      acc['maturityYieldPercent'] += (item.cells.quantity.value * item.cells.maturityYield)/rows.length;
      acc['annualYield'] += (item.cells.quantity.value * item.cells.annualYield)/rows.length;

      return acc;
    }, defaultSummary);

    // @RM: figure out the better way of doing that
    sRow['couponYield'] = roundToTwoDecimal(sRow['couponYield']);
    sRow['maturityYield'] = roundToTwoDecimal(sRow['maturityYield']);
    sRow['maturityYieldPercent'] = roundToTwoDecimal(sRow['maturityYieldPercent']);
    sRow['annualYield'] = roundToTwoDecimal(sRow['annualYield']);

    var summaryRowArr = [];

    var placeHolder = document.createElement('tr');
    placeHolder.classList.add('eurobonds-computations__summary-placeholder');

    var dataRow = document.createElement('tr');
    dataRow.classList.add('eurobonds-computations__summary-row');

    _.each(sRow, function(val, key) {
      var td = document.createElement('td');
      td.classList.add('eurobonds-computations__summary-cell');
      td.innerHTML = val;
      dataRow.appendChild(td);
    });

    summaryRowArr.push(placeHolder, dataRow);

    return summaryRowArr;
  }

  return {
    'init': function(rowsData, options) {
      var $container = $('.eurobonds-computations'),
          $deleteBtn = $('.bonds-delete', $container),
          $showBtn   = $('[data-toggle="bonds-computations"]'),
          $formView  = $('.eurobonds-computations__form', $container),
          $tableView = $('.eurobonds-computations__table', $container);

      // initialize the secondary table
      var compTable = new EurobondsCompTable($tableView, {
        'baseClass': 'eurobonds-computations',
        'leverage': options.leverage,
        'responseData': options.responseData,
        'summaryTitle': $tableView.data('summaryRow')
      });

      $showBtn.removeClass('hidden');
      $showBtn.off('click').on('click', function() {
        $showBtn.toggleClass('eurobonds__subheader--active');
        $container.toggleClass('hidden');
      });


      /* delete selected rows */
      $deleteBtn.off('click').on('click', function(e) {
        e.preventDefault();
        var rowsLeft = compTable.deleteRows();
        $deleteBtn.addClass('hidden');

        // if no rows left in the state - hide the table
        if (rowsLeft === 0) {
          $tableView.addClass('hidden')
        }
      });

      $tableView.off('click');

      /* select row functionality */
      $tableView.on('click', 'tbody tr.eurobonds-computations__body-row', function(e) {
        e.preventDefault();
        var selectedLen = compTable.toggleSelectRow(e.currentTarget);

        if (selectedLen > 0) {
          $deleteBtn.removeClass('hidden');
        } else {
          $deleteBtn.addClass('hidden');
        }
      });

      /* sorting functionality */
      $tableView.on('click', '[data-sortable="true"]', function() {
        var $view = $(this),
            column = $(this).data('columnKey'),
            currentIconSel = '';

        compTable.sortByColumn(column);

        // after calling sort By Column we need to add a summary row to the table.
        compTable.render();

        currentIconSel = sprintf('[data-sort-order="%s"]', compTable.sortParam.order ? 'desc' : 'asc');

        $('[data-sort-order]', $tableView).removeClass('sort-icon--active');
        $(currentIconSel, $view).addClass('sort-icon--active');
      });

      /* prevent selecting the row by clicking on an input */
      $tableView.on('click', 'input.computation-quantity', function(e) {
        e.preventDefault();
        e.stopPropagation();
      });

      /* recalculate and re-render the table on quantity input value change */
      $tableView.off('change').on('change', 'input.computation-quantity', function(e) {
        e.preventDefault();

        var quantity = $(e.target).val();

        // test the user's value to be a number
        if ( /\D/.test(quantity) ) {
          return;
        }

        var cTableRow = $(e.target).parents('tr')[0];

        var cRowId = cTableRow._AF_rowId;

        // select the computations table's row
        var rowFromCompTable = _.find(rowsData, function(i) {
          return i.id == cRowId;
        });

        compTable.updateAmount(rowFromCompTable, quantity);
      });

    }
  }
});