requirejs.config({
  paths: {
    'jquery': '/jquery',
    'calendar': '/calendar',
    'jquery-selectric': '/jquery.selectric',
    'base-table': '/base-table',
    'eurobonds-secondary-table': '/eurobonds-secondary-table',
    'base-table': '/base-table',
    'mockF': '/mock',
    'jquery-selectric': 'jquery.selectric',
    'underscore': '/underscore'
  },
  shim: {
    'underscore': {
      exports: '_',
      init: function() {
        return _;
      }
    }
  }
})

require(['jquery', 'calendar'], function($, calendar) {
  calendar.init('eurobonds');
});
