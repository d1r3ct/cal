define(['base-table', 'eurobonds-secondary-table', 'underscore'], function(
  BaseTable,
  eurobondsComp,
) {
  'use strict';

  // it only does '%s', and return '' when arguments are undefined
  var sprintf = function (str) {
      var args = arguments,
          flag = true,
          i = 1;

      str = str.replace(/%s/g, function () {
          var arg = args[i++];

          if (typeof arg === 'undefined') {
              flag = false;
              return '';
          }
          return arg;
      });
      return flag ? str : '';
  };

  function EurobondsDesk($view, options) {
    var self = this,
        rows = [];

    // inherit from the base class
    BaseTable.call(self, $view, options);

    /*
     * If there's no rows in response, show "empty" placeholder and return
     */
    if (options.data.length === 0) {
      _placeholder('empty', {
        'table': self.view,
        'text': self.view.data('emptyText') || 'No data.',
        'className': self.classNames.placeholder
      });

      return;
    }

    $view.siblings('.' + self.classNames.placeholder).remove();
    $view.removeClass(self.classNames.tableEmpty);

    /*
     * Filter the response data in accordance with selected currency
     */
    var rowsData = _.filter(options.data, function(item) {
      return item.currency.toLowerCase() == options.currency.toLowerCase();
    });

    rowsData = _normalizeData(rowsData);

    /*
     * Loop through the filtered data and store the rows in the state
     */
    _.each(rowsData, function(rowData, idx) {
      var row = BaseTable.prototype.createTr.call(self, rowData, idx, calculateCellValue, options);
      rows.push(row);
    });

    // expose the state (probably an antipattern)
    _.extend(self, {
      'rows': rows
    });

    /*
     * Actual DOM rendering
     */
    self.render();
  }

  EurobondsDesk.prototype = Object.create(BaseTable.prototype);
  EurobondsDesk.prototype.constructor = EurobondsDesk;

  /**
   * Function applies the formula to an induvidual column
   *
   * (!) Be ware that some columns may rely on the others which makes the column
   * order really matter.
   *
   * If we want to save more data of the particular row to the state, just return an
   * object with "html" property.
   *
   * @param  {string} columnName
   * @param  {Object} bondsData
   * @param  {Array}  existingCells
   * @param  {Object} options
   *
   * @return {Object|string}
   */
  function calculateCellValue(columnName, bondsData, existingCells, options) {
    var val;

    switch(columnName) {
      case 'company':
        // in this case, we need to return an image and a title
        return {
          'title': bondsData.title,
          'html': [
            sprintf('<div class="eurobonds-desk__company-cell">'),
              sprintf('<img class="eurobonds-desk__logo" src="%s">', bondsData.companyLogo),

              sprintf('<span class="eurobonds-desk__title">%s</span>', bondsData.title),

              sprintf('<span class="eurobonds-desk__overlay">'),
                sprintf('<img src="/themes/main/assets/img/eurobonds-desk/af-check.svg"/>'),
              sprintf('</span'),
            sprintf('</div>')
          ].join('')
        }

      case 'symbol':
        return bondsData.symbol;

      case 'nominalPrice':
        return bondsData.currentAsk/options.leverage * 100/1000;

      case 'couponYield':
        return bondsData.couponYield;

      case 'maturityDate':
        var d = new Date(bondsData.expirationDate);

        // slice is used to format the number to have two digits
        var day = ('0' + d.getDay()).slice(-2),
            month = ('0' + (d.getMonth()+1)).slice(-2),
            year = '' + d.getFullYear();

        return {
          'sortData': new Date(bondsData.expirationDate).getTime(),
          'html': sprintf('%s.%s.%s', day, month, year)
        };

      case 'daysUntilMaturity':
        var current = new Date();
        var end = new Date(bondsData.expirationDate);

        return Math.round( Math.abs(end.getTime() - current.getTime()) / (24*60*60*1000) );

      case 'maturityYield':
        val = (bondsData.couponYield * existingCells.daysUntilMaturity) - ((bondsData.currentAsk/options.leverage - 1000) * options.leverage);
        return parseFloat(val).toFixed(2);

      case 'maturityYieldPercent':
        val = (existingCells.maturityYield * 100) / (bondsData.currentAsk / 5);
        return parseFloat(val).toFixed(2);

      case 'annualYield':
        val = (365 * existingCells.maturityYieldPercent) / existingCells.daysUntilMaturity;
        return parseFloat(val).toFixed(2);

      default:
        return 'Unknown column name';
    }
  }

  function _normalizeData(responseData) {
    return _.map(responseData, function(item) {
      item.couponYield = typeof item.couponYield === 'string' ? parseFloat(item.couponYield.replace(/\,/g, '.')) : item.couponYield;
      item.currentAsk  = typeof item.currentAsk === 'string' ? parseFloat(item.currentAsk.replace(/\,/g, '.')) : item.currentAsk;
      item.initAsk     = typeof item.initAsk === 'string' ? parseFloat(item.initAsk.replace(/\,/g, '.')) : item.initAsk;

      return item;
    })
  }

  function _placeholder(type, options) {
    options.table.siblings('.' + options.className).remove();

    var placeholder = document.createElement('div');
    placeholder.innerHTML = options.text;
    placeholder.classList.add(options.className);

    options.table.after(placeholder);
  }

  /**
   * Insert the table rows into the DOM
   * @param {object} state
   */
  EurobondsDesk.prototype.render = function() {
    /*
     * Render table body
     */
    var tableBody = $(this.tbody);
    tableBody.children().remove();
    var rowsToRender = [];

    _.each(this.rows, function(item, idx) {
      rowsToRender.push(item.nTr);
    });

    tableBody.append($(rowsToRender));
  }

  function buildOptions(data) {
    // <option selected disabled hidden>Не выбрано</option>
    var rowOptions = [];

    _.each(data, function(item, idx) {
      var option = document.createElement('option');

      option.classList.add('computation-option');

      option._AF_rowId = item.id;
      option._AF_rowIndex = idx;
      option.innerHTML = item.cells.company.title;

      rowOptions.push(option);
    });

    return rowOptions;
  }

  return {
    init: function(baseClass) {

      var responseDataCache = [],
          $main           = $('.eurobonds'),
          $container      = $('.eurobonds__container', $main),
          $leverageTabs   = $('.eurobonds__leverage', $container),
          $currencyTabs   = $('.eurobonds__currency', $container),
          $primaryTable   = $('.eurobonds-desk', $container);

      var mockF={};mockF={firstLev:[{id:1,title:"Alfa-Bank-21 USD 7.3/4",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"A21#1_VTB_PERP_USD",expirationDate:"2021-04-28T00:00:00.000000+03:00",initAsk:"1255,2",currentAsk:"1155,2",couponYield:"0,21",currency:"USD"},{id:2,title:"VEB-22 USD 6.025",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"V22#1_VEB_2022_USD",expirationDate:"2022-07-05T00:00:00.000000+03:00",initAsk:"1360",currentAsk:"1090",couponYield:"0,16",currency:"USD"},{id:3,title:"VTB Perpetual USD",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"VT9#1_VTB_PERP_USD",expirationDate:"2049-12-29T00:00:00.000000+03:00",initAsk:"1150",currentAsk:"1130",couponYield:"0,26",currency:"USD"},{id:4,title:"Gazprombank Perpetual",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"GB9#1_GAZPROMBANK_PERP_USD",expirationDate:"2049-12-29T00:00:00.000000+03:00",initAsk:"1330",currentAsk:"1047",couponYield:"0,21",currency:"USD"},{id:5,title:"Sberbank 23 USD 5.25",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"SB3#1_SBERBANK_2023_USD",expirationDate:"2023-05-23T00:00:00.000000+03:00",initAsk:"1999",currentAsk:"1050",couponYield:"0,14",currency:"USD"},{id:6,title:"HSBC Perpetual USD 5.625",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"HU9#1_HSBC_PERP_USD",expirationDate:"2049-12-29T00:00:00.000000+03:00",initAsk:"1799",currentAsk:"1050",couponYield:"0,15",currency:"USD"},{id:7,title:"Standard Chartered USD 5.2",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"CU4#1_STANDARD_CHART_2024_USD",expirationDate:"2024-01-26T00:00:00.000000+03:00",initAsk:"1500",currentAsk:"1095",couponYield:"0,14",currency:"USD"}],secondLev:[{id:8,title:"Alfa-Bank-21 USD 7/4",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"VT9#2_VTB_PERP_USD",expirationDate:"2020-04-28T00:00:00.000000+03:00",initAsk:"2152,8",currentAsk:"2261.00000000",couponYield:"22",currency:"USD"},{id:9,title:"VEB-22 USD 6.025",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"VT2#2_VTB_2020_USD",expirationDate:"2021-04-28T00:00:00.000000+03:00",initAsk:"2152,8",currentAsk:"2261.00000000",couponYield:"23,27",currency:"USD"},{id:10,title:"Gazprom 2-22 USD/5.3",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"V22#2_VEB_2022_USD",expirationDate:"2019-04-28T00:00:00.000000+03:00",initAsk:"2152,8",currentAsk:"2261.00000000",couponYield:"23,27",currency:"USD"},{id:11,title:"VEB-22 USD 6.025",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"VT2#2_VTB_2020_USD",expirationDate:"2021-04-28T00:00:00.000000+03:00",initAsk:"2152,8",currentAsk:"2261.00000000",couponYield:"23,27",currency:"USD"},{id:12,title:"VEB-22 USD 6.025",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"VT2#2_VTB_2020_USD",expirationDate:"2021-04-28T00:00:00.000000+03:00",initAsk:"2152,8",currentAsk:"2261.00000000",couponYield:"23,27",currency:"USD"},{id:13,title:"VEB-22 USD 6.025",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"VT2#2_VTB_2020_USD",expirationDate:"2021-04-28T00:00:00.000000+03:00",initAsk:"2152,8",currentAsk:"2261.00000000",couponYield:"23,27",currency:"USD"},{id:14,title:"VEB-22 USD 6.025",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"VT2#2_VTB_2020_USD",expirationDate:"2021-04-28T00:00:00.000000+03:00",initAsk:"2152,8",currentAsk:"2261.00000000",couponYield:"23,27",currency:"USD"},{id:15,title:"VEB-22 USD 6.025",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"VT2#2_VTB_2020_USD",expirationDate:"2021-04-28T00:00:00.000000+03:00",initAsk:"2152,8",currentAsk:"2261.00000000",couponYield:"23,27",currency:"USD"},{id:16,title:"VEB-22 USD 6.025",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"VT2#2_VTB_2020_USD",expirationDate:"2021-04-28T00:00:00.000000+03:00",initAsk:"2152,8",currentAsk:"2261.00000000",couponYield:"23,27",currency:"USD"},{id:17,title:"VEB-22 USD 6.025",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"VT2#2_VTB_2020_USD",expirationDate:"2021-04-28T00:00:00.000000+03:00",initAsk:"2152,8",currentAsk:"2261.00000000",couponYield:"23,27",currency:"USD"},{id:18,title:"Gazprom 2-22 USD/5.3",companyLogo:"path/to/img.png",symbol:"B21",expirationDate:"2021-04-28T00:00:00.000000+03:00",initAsk:"2152,8",currentAsk:"2261.00000000",couponYield:"23,27",currency:"EUR"}],fifthLev:[{id:19,title:"Alfa-Bank-21 USD 7/4",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"VT9#5_VTB_PERP_USD",expirationDate:"2020-04-28T00:00:00.000000+03:00",initAsk:"2152,8",currentAsk:"2261.00000000",couponYield:"22",currency:"USD"},{id:20,title:"VEB-22 USD 6.025",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"VT2#5_VTB_2020_USD",expirationDate:"2021-04-28T00:00:00.000000+03:00",initAsk:"2152,8",currentAsk:"2261.00000000",couponYield:"23,27",currency:"USD"},{id:21,title:"Gazprom 2-22 USD/5.3",companyLogo:"http://alfaforex.crtweb.ru/storage/app/uploads/public/59f/c68/e21/thumb_208_50_50_0_0_auto.png",symbol:"V22#5_VEB_2022_USD",expirationDate:"2019-04-15T00:00:00.000000+03:00",initAsk:"2152,8",currentAsk:"2261.00000000",couponYield:"23,27",currency:"USD"},{id:22,title:"Gazprom 2-22 USD/5.3",companyLogo:"path/to/img.png",symbol:"B21",expirationDate:"2021-04-28T00:00:00.000000+03:00",initAsk:"2152,8",currentAsk:"2261.00000000",couponYield:"23,27",currency:"EUR"}],getInitData:function(t){switch(t){case 5:return this.fifthLev;case 2:return this.secondLev;case 1:default:return this.firstLev}}};

      function getBonds() {
        var lvrg = $container.data('currentLeverage');
        initialize(mockF.getInitData(lvrg));
        responseDataCache = mockF.getInitData(lvrg);

        /*$.ajax({
          'url': '/bonds-desc/' + $container.data('currentLeverage')
        }).done(function(response) {
          responseDataCache = response.data;
          initialize(response.data);
        })*/
      }

      var table = new EurobondsDesk($primaryTable, {
        'data': mockF.getInitData($container.data('currentLeverage')),
        'baseClass': 'eurobonds-desk',
        'leverage': $container.data('currentLeverage'),
        'currency': $container.data('currentCurrency')
      });

      /* initialize the computation (secondary table) from the data of the
       * primary table */
      eurobondsComp.init(table.rows, {
        'leverage': Number($container.data('currentLeverage')),
        'responseData': mockF.getInitData($container.data('currentLeverage'))
      });

      // __________ SELECT FORM ____________

      var selectInput = $('[data-target="eurobonds"]');

      // remove the previously built custom options
      selectInput.children('.computation-option').remove();

      var selectOptions = buildOptions(table.rows);
      $(selectOptions).appendTo(selectInput);

      selectInput.selectric();

            /* on form submission add the row to the computation table*/
      $formView.off('submit').on('submit', function(e) {
        e.preventDefault();
        var $select = $('select', this),
            $amount = $('[type="number"]', this);

        var selectedOptionIdx = $select[0].selectedIndex;

        if (selectedOptionIdx === 0) {
          return false;
        }

        var currentOption = selectOptions[selectedOptionIdx-1];

        var quantity = $amount.val();
        quantity = quantity ? Number(quantity) : 1;
        var primaryRowId = currentOption._AF_rowId;
        var primaryRowIndex = currentOption._AF_rowIndex;

        var isBondExist = _.some(compTable.rows, function(row) {
          return row.id === primaryRowId;
        });

        // select the primary table's row
        var primaryRow = rowsData[primaryRowIndex];

        if (isBondExist) {
          // if row is already inside the second table, we only want to increase its quantity
          compTable.updateAmount(primaryRow, quantity, true);
        } else {
          // otherwise append it to the table
          compTable.appendRow(primaryRow, quantity);
        }

        $tableView.removeClass('hidden');

        return false;
      });

      // _________________________

      $primaryTable.on('click', '[data-sortable="true"]', function() {
        var $view = $(this),
            column = $(this).data('columnKey'),
            currentIconSel = '';

        table.sortByColumn(column);
        table.render();

        currentIconSel = sprintf('[data-sort-order="%s"]', table.sortParam.order ? 'desc' : 'asc');

        $('[data-sort-order]', $primaryTable).removeClass('sort-icon--active');
        $(currentIconSel, $view).addClass('sort-icon--active');
      });

      $leverageTabs.off('click').on('click', function(e) {
        e.preventDefault();
        var $tab = $(this);
        var leverage = $tab.data('tabLeverage');
        var currentLeverage = $container.data('currentLeverage');

        if (currentLeverage !== leverage) {
          $container.data('currentLeverage', leverage);
          $leverageTabs.removeClass('eurobonds__leverage--active');
          $tab.addClass('eurobonds__leverage--active');
          getBonds(leverage);
        }
      });

      $currencyTabs.off('click').on('click', function(e) {
        e.preventDefault();

        var $tab = $(this),
            currency = $tab.data('tabCurrency'),
            currentCurrency = $container.data('currentCurrency');

        if (currentCurrency != currency) {
          $container.data('currentCurrency', currency);
          $currencyTabs.removeClass('eurobonds__currency--active');
          $tab.addClass('eurobonds__currency--active');
          initialize(responseDataCache);
        }
      });

      function initialize(data) {
        var table = new EurobondsDesk($primaryTable, {
          'data': data,
          'baseClass': 'eurobonds-desk',
          'leverage': $container.data('currentLeverage'),
          'currency': $container.data('currentCurrency')
        });

        // if the table doesn't contain any data, just return
        if (table.rows.length === 0) {
          return;
        }
      }

    }
  }

})
